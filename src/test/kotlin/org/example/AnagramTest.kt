package org.example

import org.junit.Test
import kotlin.test.assertTrue
import kotlin.test.assertFalse

class AnagramTest {
    private val anagramUtils = AnagramUtils()

    @Test
    fun `an element is an anagram of itself` () {
        assertTrue { anagramUtils.isAnagram("a", "a") }
        assertTrue { anagramUtils.isAnagram("a b", "a b") }
        assertTrue { anagramUtils.isAnagram("ab c", "ab c") }
    }

    @Test
    fun `whitespace is not relevant for an anagram`() {
        assertTrue { anagramUtils.isAnagram("ab", "a b") }
        assertTrue { anagramUtils.isAnagram("ab", "a b ") }
        assertTrue { anagramUtils.isAnagram("ab", "a   b ") }
        assertTrue { anagramUtils.isAnagram("ab", "  a   b ") }
    }

    @Test
    fun `capitalization is not relevant for an anagram`() {
        assertTrue { anagramUtils.isAnagram("AB", "ab") }
        assertTrue { anagramUtils.isAnagram("AB", "A b") }
        assertTrue { anagramUtils.isAnagram("aB", "a b") }
        assertTrue { anagramUtils.isAnagram("ab", "A B") }
    }
    @Test
    fun `punctuation is not relevant for an anagram`() {
        assertTrue { anagramUtils.isAnagram("AB!", "ab") }
        assertTrue { anagramUtils.isAnagram("A, B", "A b") }
        assertTrue { anagramUtils.isAnagram("aB.", "a b") }
        assertTrue { anagramUtils.isAnagram("a , ! b?", "A B") }
    }

    @Test
    fun `a word or sentence containing different letters is not an anagram `() {
        assertFalse{ anagramUtils.isAnagram("a", "b") }
        assertFalse{ anagramUtils.isAnagram("a", "ab") }
        assertFalse{ anagramUtils.isAnagram("ab", "abc") }
        assertFalse{ anagramUtils.isAnagram("a a", "a b") }
    }

}
