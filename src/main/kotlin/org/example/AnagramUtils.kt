package org.example

class AnagramUtils {

    fun isAnagram (string1: String, string2: String):Boolean {
        fun sanitize(string: String) = string
            .toLowerCase()
            .replace("\\s+".toRegex(), "") // Remove whitespace
            .replace("\\p{Punct}".toRegex(), "") // Remove punctuation
            .toList().sorted()

        return sanitize(string1) == sanitize(string2)
    }

}