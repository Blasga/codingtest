package org.example

fun main(args: Array<String>) {
    val anagramUtils = AnagramUtils()

    val string1 = "this is an anagram"
    val string2 = "an anagram this is"

    println(
        anagramUtils.isAnagram(string1, string2 )
    )
}

